class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calcula_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
                tax=self.calcula_impuestos())


class Jefe (Empleado):
    def __init__(self, n, s, p):
        super().__init__(n,s) #'Padre' construye tu esto con los parámetros que tienes y yo me encargo de añadir lo demás.
        self.prima = p #nuevo atributo

    def calcula_impuestos (self):
        return super().calcula_impuestos() + self.prima*0.30
    
    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format (name=self.nombre, tax=self.calcula_impuestos())
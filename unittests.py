#!/usr/bin/env python3

import unittest
from empleado import Empleado
from empleado import Jefe

class TestEmpleado(unittest.TestCase):

    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calcula_impuestos(), 1500)
   
    def test_str(self):
        e1 = Empleado("Pepe",50000)
        self.assertEqual("El empleado Pepe debe pagar 15000.00", e1.__str__())

class TestJefe(unittest.TestCase):
   
    def test_prima(self):
        el = Jefe ("Elena", 30000, 2000)
        self.assertEqual (el.prima ,2000)
    
    def test_impuestos(self):
        el = Jefe ("Elena", 30000, 2000)
        self.assertEqual (el.calcula_impuestos(),9600.00)
    
    def test_str(self):
        el = Jefe ("Elena",30000, 2000)
        self.assertEqual ("El jefe Elena debe pagar 9600.00", el.__str__())


if __name__ == "__main__":
    unittest.main()